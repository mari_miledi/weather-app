//
//  BaseService.swift
//  WeatherKit
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import RxSwift

class BaseService {
    
    private static let sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        let sessionManager = Alamofire.SessionManager(configuration: configuration)
        
        return sessionManager
    }()
    
    func request<T: Mappable>(_ url: URLConvertible,
                              method: HTTPMethod = .get,
                              parameters: Parameters? = nil,
                              encoding: ParameterEncoding = URLEncoding.default,
                              headers: HTTPHeaders? = nil) -> Single<T> {
        return Single.create { (observer) -> Disposable in
            let request = BaseService
                .sessionManager
                .request(url,
                         method: method,
                         parameters: parameters,
                         encoding: encoding,
                         headers: headers)
                .responseJSON(completionHandler: { (response: DataResponse<Any>) in
                    guard let json = response.result.value as? [String: Any] else {
                        observer(.error(NetworkError.serverError))
                        return
                    }
                    
                    guard let object = Mapper<T>().map(JSON: json) else {
                        observer(.error(NetworkError.parsingError))
                        return
                    }
                    
                    observer(.success(object))
                })
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
}
