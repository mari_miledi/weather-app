//
//  WeatherService.swift
//  WeatherKit
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

private enum Route {
    case
    
    getForecast(latitude: Double, longitude: Double, apiKey: String)
    
    var path: String {
        switch self {
        case .getForecast(let latitude, let longitude, let apiKey):
            return "forecast/\(apiKey)/\(latitude),\(longitude)?exclude=currently,minutely,hourly,flags&units=si"
        }
    }
}

final class WeatherService: BaseService {
    
    static let shared = WeatherService(baseURL: "https://api.darksky.net/")
    
    private let baseURL: String
    private let apiKey: String = "2bf8efaa0758eb998a32674fa97be4cf"
    
    init(baseURL: String) {
        self.baseURL = baseURL
    }
    
    private func fullUrl(for route: Route) -> String {
        return "\(baseURL)\(route.path)"
    }
    
    func getForecast(latitude: Double, longitude: Double) -> Single<ServerForecast> {
        let path = fullUrl(for: Route.getForecast(latitude: latitude, longitude: longitude, apiKey: apiKey))
        
        return request(path)
    }
}
