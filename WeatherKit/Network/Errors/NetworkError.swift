//
//  NetworkError.swift
//  WeatherKit
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation

enum NetworkError: LocalizedError {
    case serverError
    case parsingError
    
    var errorDescription: String? {
        switch self {
        case .serverError:
            return "ERROR_CANNOT_LOAD_DATA"
        case .parsingError:
            return "Parsing error"
        }
    }
}
