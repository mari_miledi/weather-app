//
//  ServerForecast.swift
//  WeatherKit
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation
import ObjectMapper
import CleanroomLogger

public struct ServerForecast: Mappable {
    
    public var latitude: Double
    public var longitude: Double
    public var timeZone: String
    public var summary: String?
    public var daily: [ServerDailyForecast] = []
    
    public init?(map: Map) {
        guard
            let latitude = map.JSON["latitude"] as? Double,
            let longitude = map.JSON["longitude"] as? Double,
            let timeZone = map.JSON["timezone"] as? String
            else {
                Log.error?.message("Error while parsing forecast: \(map.JSON)")
                return nil
        }
        
        self.latitude = latitude
        self.longitude = longitude
        self.timeZone = timeZone
    }
    
    public mutating func mapping(map: Map) {
        daily <- map["daily.data"]
        summary <- map["daily.summary"]
    }
}
