//
//  DailyForecast.swift
//  WeatherKit
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation
import ObjectMapper
import CleanroomLogger

public struct ServerDailyForecast: Mappable {
    
    public var date: Date?
    public var summary: String
    public var icon: String
    public var temperatureMin: Double
    public var temperatureMax: Double
    public var precipitationProbability: Double
    public var humidity: Double
    public var pressure: Double
    public var windSpeed: Double
    public var uvIndex: Int
    public var visibility: Double
    
    public init?(map: Map) {
        guard
            let summary = map.JSON["summary"] as? String,
            let icon = map.JSON["icon"] as? String,
            let temperatureMin = map.JSON["temperatureLow"] as? Double,
            let temperatureMax = map.JSON["temperatureHigh"] as? Double,
            let precipitationProbability = map.JSON["precipProbability"] as? Double,
            let humidity = map.JSON["humidity"] as? Double,
            let pressure = map.JSON["pressure"] as? Double,
            let windSpeed = map.JSON["windSpeed"] as? Double,
            let uvIndex = map.JSON["uvIndex"] as? Int,
            let visibility = map.JSON["visibility"] as? Double
            else {
                Log.error?.message("Error while parsing daily forecast: \(map.JSON)")
                return nil
        }
        
        self.summary = summary
        self.icon = icon
        self.temperatureMin = temperatureMin
        self.temperatureMax = temperatureMax
        self.precipitationProbability = precipitationProbability
        self.humidity = humidity
        self.pressure = pressure
        self.windSpeed = windSpeed
        self.uvIndex = uvIndex
        self.visibility = visibility
    }
    
    public mutating func mapping(map: Map) {
        date <- (map["time"], TicksDateTransform())
    }
}
