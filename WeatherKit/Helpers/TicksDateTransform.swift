//
//  TicksDateTransform.swift
//  WeatherKit
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation
import ObjectMapper

class TicksDateTransform: TransformType {
    typealias Object = Date
    typealias JSON = Int
    
    public func transformFromJSON(_ value: Any?) -> TicksDateTransform.Object? {
        guard let timeInterval = value as? Double else { return nil }
        
        return Date(timeIntervalSince1970: TimeInterval(timeInterval))
    }
    
    public func transformToJSON(_ value: TicksDateTransform.Object?) -> TicksDateTransform.JSON? {
        guard let value = value else { return nil }
        
        return Int(value.timeIntervalSince1970)
    }
}
