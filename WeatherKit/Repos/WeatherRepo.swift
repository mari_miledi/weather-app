//
//  WeatherRepo.swift
//  WeatherKit
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation
import RxSwift
import CoreStore
import CleanroomLogger

public class WeatherRepo {
    
    public static let shared = WeatherRepo()
    
    public func getForecast(latitude: Double, longitude: Double) -> Single<Forecast> {
        return WeatherService.shared
            .getForecast(latitude: latitude, longitude: longitude)
            .flatMap { self.saveForecast($0) }
    }
    
    private func saveForecast(_ serverForecast: ServerForecast) -> Single<Forecast> {
        return Single<Forecast>.create(subscribe: { (observer) -> Disposable in
            CoreStore.perform(asynchronous: { (transaction) -> Forecast in
                transaction.deleteAll(From<Forecast>())
                
                let forecast = Forecast.createForecast(from: serverForecast, transaction: transaction)
                let dailyForecasts = DailyForecast.createDailyForecasts(from: serverForecast.daily, transaction: transaction)
                forecast.addToDailyForecasts(NSSet(array: dailyForecasts))
                
                return forecast
            }, completion: { (result) in
                switch result {
                case .success(let forecast):
                    Log.debug?.message("Forecast successfully saved")
                    observer(.success(forecast))
                case .failure(let error):
                    Log.error?.message("Error while saving forecast: \(error.localizedDescription)")
                    observer(.error(error))
                }
            })
            
            return Disposables.create()
        })
    }
}
