//
//  DailyForecast.swift
//  WeatherKit
//
//  Created by Mariia on 10/9/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import CoreStore

public class DailyForecast: NSManagedObject {
    
    @discardableResult
    static func createDailyForecasts(from serverDailyForecasts: [ServerDailyForecast],
                                     transaction: AsynchronousDataTransaction) -> [DailyForecast] {
        var dailyForecasts = [DailyForecast]()
        for serverDailyForecast in serverDailyForecasts {
            let dailyForecast = transaction.create(Into<DailyForecast>(nil))
            dailyForecast.setup(from: serverDailyForecast,in: transaction)
            dailyForecasts.append(dailyForecast)
        }
        
        return dailyForecasts
    }
    
    private func setup(from serverDailyForecast: ServerDailyForecast,
                       in transaction: AsynchronousDataTransaction) {
        self.date = serverDailyForecast.date
        self.summary = serverDailyForecast.summary
        self.icon = serverDailyForecast.icon
        self.temperatureMin = serverDailyForecast.temperatureMin
        self.temperatureMax = serverDailyForecast.temperatureMax
        self.precipitationProbability = serverDailyForecast.precipitationProbability
        self.humidity = serverDailyForecast.humidity
        self.pressure = serverDailyForecast.pressure
        self.windSpeed = serverDailyForecast.windSpeed
        self.uvIndex = Int16(serverDailyForecast.uvIndex)
        self.visibility = serverDailyForecast.visibility
    }
}
