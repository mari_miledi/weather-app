//
//  Forecast.swift
//  WeatherKit
//
//  Created by Mariia on 10/9/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import CoreStore

public class Forecast: NSManagedObject {
    
    @discardableResult
    static func createForecast(from serverForecast: ServerForecast,
                               transaction: AsynchronousDataTransaction) -> Forecast {
        let forecast = transaction.create(Into<Forecast>(nil))
        forecast.setup(from: serverForecast, in: transaction)
        
        return forecast
    }
    
    private func setup(from serverForecast: ServerForecast,
                       in transaction: AsynchronousDataTransaction) {
        self.latitude = serverForecast.latitude
        self.longitude = serverForecast.longitude
        self.summary = serverForecast.summary
        self.timeZone = serverForecast.timeZone
    }
}
