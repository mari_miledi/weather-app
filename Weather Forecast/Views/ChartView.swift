//
//  ChartView.swift
//  Weather Forecast
//
//  Created by Mariia on 10/9/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import UIKit

class ChartView: UIView {

    // MARK: - Constants
    
    private static let dotRadius: CGFloat = 3.0
    private static let lineWidth: CGFloat = 2.0
    private static let lineStep: Double = 5.0
    private static let margin: CGFloat = 15.0
    private static let cornerRadius: CGFloat = 5.0
    
    // MARK: - Public properties
    
    var tapHandler: ((Int) -> Void)?
    
    // MARK: - Private properties
    
    private var values: [Double] = []
    private var weekdays: [String] = []
    private var dotsCenterPoints: [CGPoint] = []
    private var horizontalRegionWidth: CGFloat!
    private var minValue: Double!
    private var maxValue: Double!
    private var minHorizontalLineValue: Double!
    private var maxHorizontalLineValue: Double!
    private var horizontalLinesCount: Double!
    private var chartHeight: Double!
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: - Public methods
    
    func setup(with temperatureValues: [Double], weekdays: [String]) {
        values = temperatureValues
        self.weekdays = weekdays
        
        let sortedValues = temperatureValues.sorted()
        horizontalRegionWidth = (bounds.width - ChartView.margin * 2) / CGFloat(values.count)
        minValue = round(sortedValues.first ?? 0)
        maxValue = round(sortedValues.last ?? 0)
        minHorizontalLineValue = minValue - (minValue.truncatingRemainder(dividingBy: ChartView.lineStep))
        if minHorizontalLineValue == minValue || minValue < 0 {
            minHorizontalLineValue -= ChartView.lineStep
        }
        maxHorizontalLineValue = maxValue - (maxValue.truncatingRemainder(dividingBy: ChartView.lineStep)) + ChartView.lineStep
        if maxHorizontalLineValue == maxValue || maxValue < 0 {
            maxHorizontalLineValue += ChartView.lineStep
        }
        horizontalLinesCount = (maxHorizontalLineValue - minHorizontalLineValue) / ChartView.lineStep + 1
        chartHeight = maxHorizontalLineValue - minHorizontalLineValue
        
        setNeedsDisplay()
    }
    
    // MARK: - Private methods
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first, touches.count == 1 else { return }
        
        tapHandler?(indexOfNearestDot(from: touch.location(in: self)))
    }
    
    private func commonInit() {
        contentMode = .redraw
        layer.contentsScale = UIScreen.main.scale
    }
    
    private func indexOfNearestDot(from point: CGPoint) -> Int {
        return Int(floor((point.x - ChartView.margin) / horizontalRegionWidth))
    }
    
    private func calculateDotsCenterPoints() {
        guard !values.isEmpty else { return }
        
        let height = bounds.height - ChartView.margin * 4
        for (index, value) in values.enumerated() {
            let x = ChartView.margin + horizontalRegionWidth / 2 + CGFloat(index) * horizontalRegionWidth
            let y = ChartView.margin * 2 + height - height * CGFloat((round(value) - minHorizontalLineValue) / chartHeight)
            dotsCenterPoints.append(CGPoint(x: x, y: y))
        }
    }
    
    // MARK: - Drawing
    
    override func draw(_ rect: CGRect) {
        guard !values.isEmpty, !weekdays.isEmpty else { return }
        
        dotsCenterPoints.removeAll()
        calculateDotsCenterPoints()
        
        drawBackground()
        drawHorizontalLinesAndValues()
        drawVerticalLines()
        drawChartLine()
        drawDots()
        drawWeekdays()
    }
    
    private func drawBackground() {
        let path = UIBezierPath(roundedRect: CGRect(x: ChartView.margin,
                                                    y: ChartView.margin,
                                                    width: bounds.width - ChartView.margin * 2,
                                                    height: bounds.height - ChartView.margin * 2),
                                cornerRadius: ChartView.margin)
        path.close()
        #colorLiteral(red: 0.7252383828, green: 0.8730613589, blue: 1, alpha: 1).setFill()
        path.fill()
    }
    
    private func drawHorizontalLinesAndValues() {
        let path = UIBezierPath()
        let count = Int(exactly: horizontalLinesCount)!
        let distanceBetweenLines = (bounds.height - ChartView.margin * 4) / CGFloat(count - 1)
        let font = UIFont.systemFont(ofSize: 13.0)
        let attributes: [NSAttributedStringKey: Any] = [.foregroundColor: UIColor.white,
                                                        .font: font]
        let textHeight = String(round(values[0])).heightWithConstrainedWidth(width: 20, font: font)
        
        for index in 0..<count {
            let y = ChartView.margin * 2 + distanceBetweenLines * CGFloat(index)
            path.move(to: CGPoint(x: dotsCenterPoints.first!.x, y: y))
            path.addLine(to: CGPoint(x: dotsCenterPoints.last!.x, y: y))
            
            let text = String(Int(round(maxHorizontalLineValue - Double(index) * ChartView.lineStep)))
            let point = CGPoint(x: bounds.width - ChartView.margin * 2, y: y - textHeight / 2)
            NSAttributedString(string: text, attributes: attributes).draw(at: point)
        }
        path.close()
        UIColor.white.withAlphaComponent(0.5).setStroke()
        path.stroke()
    }
    
    private func drawVerticalLines() {
        let path = UIBezierPath()
        for index in 0...values.count {
            let x = ChartView.margin + horizontalRegionWidth * CGFloat(index)
            path.move(to: CGPoint(x: x, y: ChartView.margin))
            path.addLine(to: CGPoint(x: x, y: bounds.height - ChartView.margin))
        }
        path.close()
        UIColor.white.withAlphaComponent(0.3).setStroke()
        path.stroke()
    }
    
    private func drawDots() {
        let path = UIBezierPath()
        for point in dotsCenterPoints {
            path.move(to: point)
            path.addArc(withCenter: point,
                        radius: ChartView.dotRadius,
                        startAngle: 0,
                        endAngle: 2 * CGFloat.pi,
                        clockwise: true)
        }
        UIColor.white.setFill()
        path.close()
        path.fill()
    }
    
    private func drawChartLine() {
        let path = UIBezierPath()
        for (index, point) in dotsCenterPoints.enumerated() {
            if index == 0 {
                path.move(to: point)
            } else {
                path.addLine(to: point)
            }
        }
        path.move(to: dotsCenterPoints[0])
        path.close()
        path.lineWidth = 1
        UIColor.white.setStroke()
        path.stroke()
    }
    
    private func drawWeekdays() {
        for (index, weekday) in weekdays.enumerated() {
            let font = UIFont.systemFont(ofSize: 13.0)
            let attributes: [NSAttributedStringKey: Any] = [.foregroundColor: UIColor.black,
                                                            .font: font]
            let textWidth = weekday.widthWithConstrainedHeight(height: 20, font: font)
            let point = CGPoint(x: dotsCenterPoints[index].x - textWidth / 2, y: 0)
            NSAttributedString(string: weekday, attributes: attributes).draw(at: point)
        }
    }
}
