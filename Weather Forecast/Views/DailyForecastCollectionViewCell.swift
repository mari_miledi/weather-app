//
//  DailyForecastCollectionViewCell.swift
//  Weather Forecast
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import UIKit
import WeatherKit

class DailyForecastCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var temperatureTitleLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var precipitationProbabilityTitleLabel: UILabel!
    @IBOutlet weak var precipitationProbabilityLabel: UILabel!
    @IBOutlet weak var humidityTitleLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureTitleLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windSpeedTitleLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var uvIndexTitleLabel: UILabel!
    @IBOutlet weak var uvIndexLabel: UILabel!
    @IBOutlet weak var visibilityTitleLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    
    private var calendar: Calendar = {
        var calendar = Calendar.autoupdatingCurrent
        calendar.locale = Locale.autoupdatingCurrent
        
        return calendar
    }()
    private static var percentFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        
        return formatter
    }()
    private static var temperatureFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0
        
        return formatter
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconImageView.tintColor = .white
        containerView.layer.rasterizationScale = UIScreen.main.scale
        containerView.layer.shouldRasterize = true
        containerView.layer.cornerRadius = 5.0
        containerView.layer.masksToBounds = false
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.1
    }

    func setup(with dailyForecast: DailyForecast, timeZone: TimeZone) {
        if let date = dailyForecast.date {
            calendar.timeZone = timeZone
            let weekday = calendar.component(.weekday, from: date)
            let weekdayName = calendar.standaloneWeekdaySymbols[weekday - 1]
            weekdayLabel.text = weekdayName
        }
        
        temperatureLabel.attributedText = temperatureString(dailyForecast)
        iconImageView.image = (IconType(rawValue: dailyForecast.icon ?? String()) ?? .clear).image
        let precipitationProbability = DailyForecastCollectionViewCell
            .percentFormatter
            .string(from: NSNumber(value: dailyForecast.precipitationProbability * 100)) ?? String()
        precipitationProbabilityLabel.text = "\(precipitationProbability)%"
        let humidity = DailyForecastCollectionViewCell
            .percentFormatter
            .string(from: NSNumber(value: dailyForecast.humidity * 100)) ?? String()
        humidityLabel.text = "\(humidity)%"
        pressureLabel.text = "\(dailyForecast.pressure)"
        windSpeedLabel.text = "\(dailyForecast.windSpeed)"
        uvIndexLabel.text = "\(dailyForecast.uvIndex)"
        visibilityLabel.text = "\(dailyForecast.visibility)"
    }
    
    private func temperatureString(_ dailyForecast: DailyForecast) -> NSAttributedString {
        let temperature = NSMutableAttributedString()
        let maxTemperature = DailyForecastCollectionViewCell
            .temperatureFormatter
            .string(from: NSNumber(value: dailyForecast.temperatureMax)) ?? String()
        temperature.append(NSAttributedString(string: "\(maxTemperature)°",
            attributes: [.foregroundColor: UIColor.black]))
        let minTemperature = DailyForecastCollectionViewCell
            .temperatureFormatter
            .string(from: NSNumber(value: dailyForecast.temperatureMin)) ?? String()
        temperature.append(NSAttributedString(string: " "))
        temperature.append(NSAttributedString(string: "\(minTemperature)°",
            attributes: [.foregroundColor: UIColor.black.withAlphaComponent(0.5)]))
        
        return temperature
    }
}
