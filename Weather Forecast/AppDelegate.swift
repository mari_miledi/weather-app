//
//  AppDelegate.swift
//  Weather Forecast
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import UIKit
import CoreStore
import CleanroomLogger

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupCoreDataStack()
        setupMainViewController()
        setupLogger()
        
        return true
    }
    private func setupLogger() {
        Log.enable(minimumSeverity: .debug, debugMode: true)
    }
    
    private func setupMainViewController() {
        window = UIWindow()
        window?.rootViewController = UINavigationController(rootViewController: WeatherViewController.create()) 
        window?.makeKeyAndVisible()
    }
    
    private func setupCoreDataStack() {
        CoreStore.defaultStack = DataStack(xcodeModelName: "WeatherForecast",
                                           bundle: Bundle(identifier: "com.mariiakarnaukhova.WeatherKit")!)
        let store = SQLiteStore(fileURL: storeFileURL(), localStorageOptions: .recreateStoreOnModelMismatch)
        
        Log.debug?.message(store.fileURL.absoluteString)
        
        do {
            try CoreStore.addStorageAndWait(store)
        } catch {
            Log.error?.message("Cannot create database: \(error.localizedDescription)")
        }
    }
    
    private func storeFileURL() -> URL {
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        
        return URL(fileURLWithPath: documentsDirectory).appendingPathComponent("WeatherForecast.sqlite")
    }
}

