//
//  ViewController.swift
//  Weather Forecast
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import UIKit
import MBProgressHUD
import RxSwift
import RxCocoa

final class WeatherViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.dataSource = self
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: 220, height: 250)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            collectionView.collectionViewLayout = layout
            let cellName = String(describing: DailyForecastCollectionViewCell.self)
            collectionView.register(UINib(nibName: cellName, bundle: nil), forCellWithReuseIdentifier: cellName)
        }
    }
    @IBOutlet weak var chartView: ChartView!
    
    // MARK: - Private properties
    
    private let viewModel = WeatherViewModel()
    private let bag = DisposeBag()
    
    // MARK: - IBActions
    
    @IBAction func refresh(_ sender: UIBarButtonItem) {
        viewModel.loadForecast()
    }
    
    // MARK: - Lifecycle
    
    class func create() -> WeatherViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: String(describing: WeatherViewController.self))
        
        return viewController as! WeatherViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "APP_NAME".localized
        setupBindings()
        viewModel.requestAuthorizationIfNeeded()
    }
    
    // MARK: - Private methods
    
    private func setupBindings() {
        viewModel
            .isLoadingContent
            .bind { [weak self] value in
                guard let strongSelf = self else { return }
                
                if value {
                    let hud = MBProgressHUD.showAdded(to: strongSelf.view, animated: true)
                    hud.minShowTime = 1
                } else {
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
            }
            .disposed(by: bag)
        viewModel
            .dailyForecasts
            .bind { [weak self] dailyForecasts in
                guard let strongSelf = self else { return }
                
                strongSelf.collectionView.reloadData()
                let weekdays = dailyForecasts.map { dailyForecast -> String in
                    let index = Calendar.current.component(.weekday, from: dailyForecast.date!)
                    return Calendar.current.shortWeekdaySymbols[index - 1]
                }
                strongSelf.chartView.layoutIfNeeded()
                strongSelf.chartView.setup(with: dailyForecasts.map { $0.temperatureMin },
                                           weekdays: weekdays)
                strongSelf.chartView.tapHandler = { index in
                    strongSelf.collectionView.scrollToItem(at: IndexPath(row: index, section: 0),
                                                           at: .centeredHorizontally,
                                                           animated: true)
                }
            }
            .disposed(by: bag)
        viewModel.summary.bind(to: summaryLabel.rx.text).disposed(by: bag)
        viewModel
            .errorDescription
            .bind { [weak self] errorDescription in
                guard let strongSelf = self else { return }
                
                strongSelf.showErrorAlert(with: errorDescription)
            }
            .disposed(by: bag)
    }
    
    private func showErrorAlert(with message: String) {
        let alert = UIAlertController(title: "ERROR_ALERT_TITLE".localized, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - UICollectionViewDataSource

extension WeatherViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.dailyForecasts.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellName = String(describing: DailyForecastCollectionViewCell.self)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellName, for: indexPath) as! DailyForecastCollectionViewCell
        let dailyForecast = viewModel.dailyForecasts.value[indexPath.row]
        cell.setup(with: dailyForecast, timeZone: viewModel.timeZone)
        
        return cell
    }
}
