//
//  WeatherViewModel.swift
//  Weather Forecast
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa
import CoreStore
import WeatherKit

final class WeatherViewModel: NSObject {
    
    // MARK: - Public properties
    
    var isLoadingContent = BehaviorRelay(value: false)
    var dailyForecasts = BehaviorRelay<[DailyForecast]>(value: [])
    var summary = BehaviorRelay(value: String())
    var errorDescription = PublishRelay<String>()
    var timeZone: TimeZone!
    
    override init() {
        super.init()
        
        forecastMonitor = CoreStore.monitorList(From<Forecast>().orderBy(.ascending(\Forecast.latitude)))
        forecastMonitor.addObserver(self)
        
        if forecastMonitor.hasObjects() {
            updateUI()
        }
    }
    
    // MARK: - Private properties
    
    private let bag = DisposeBag()
    private lazy var forecastMonitor: ListMonitor<Forecast>! = {
        let monitor = CoreStore.monitorList(From<Forecast>())
        monitor.addObserver(self)
        
        return monitor
    }()
    private lazy var locationManager: CLLocationManager = {
        var locationManager = CLLocationManager()
        locationManager.delegate = self
        
        return locationManager
    }()
    private var lastUserLocation: CLLocation? {
        didSet {
            loadForecast()
        }
    }
    
    // MARK: - Public methods
    
    func requestAuthorizationIfNeeded() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            break
        case .denied:
            break
        case .authorizedAlways, .authorizedWhenInUse:
            lastUserLocation = locationManager.location
        }
    }
    
    func loadForecast() {
        guard !isLoadingContent.value, let location = locationManager.location else { return }
        
        isLoadingContent.accept(true)
        
        WeatherRepo.shared
            .getForecast(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            .subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                switch event {
                case .success:
                    strongSelf.isLoadingContent.accept(false)
                case .error(let error):
                    strongSelf.errorDescription.accept(error.localizedDescription.localized)
                    strongSelf.isLoadingContent.accept(false)
                }
            }
            .disposed(by: bag)
    }
    
    private func updateUI() {
        guard
            let forecast = forecastMonitor.objectsInAllSections().first,
            let timeZone = forecast.timeZone,
            let dailyForecasts = forecast.dailyForecasts?.allObjects as? [DailyForecast] else {
                return
        }
        
        self.timeZone = TimeZone(identifier: timeZone) ?? TimeZone.current
        self.dailyForecasts.accept(dailyForecasts.sorted(by: { $0.date! < $1.date! }))
        self.summary.accept(forecast.summary ?? String())
    }
}

// MARK: - CLLocationManagerDelegate

extension WeatherViewModel: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        
        lastUserLocation = location
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        errorDescription.accept("ERROR_CANNOT_DETERMINE_LOCATION".localized)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .denied, .restricted:
            errorDescription.accept("ERROR_CANNOT_DETERMINE_LOCATION".localized)
        case .notDetermined:
            break
        }
    }
}

// MARK: - ListObserver

extension WeatherViewModel: ListObserver {
    typealias ListEntityType = Forecast
    
    func listMonitorDidChange(_ monitor: ListMonitor<Forecast>) {
        updateUI()
    }
    
    func listMonitorDidRefetch(_ monitor: ListMonitor<Forecast>) {}
}
