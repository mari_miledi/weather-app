//
//  Enums.swift
//  Weather Forecast
//
//  Created by Mariia on 10/8/18.
//  Copyright © 2018 Mariia Karnaukhova. All rights reserved.
//

import Foundation
import WeatherKit

extension IconType {
    var image: UIImage {
        switch self {
        case .clear:
            return #imageLiteral(resourceName: "ClearDay").withRenderingMode(.alwaysTemplate)
        case .rain:
            return #imageLiteral(resourceName: "Rain").withRenderingMode(.alwaysTemplate)
        case .snow:
            return #imageLiteral(resourceName: "Snow").withRenderingMode(.alwaysTemplate)
        case .sleet:
            return #imageLiteral(resourceName: "Sleet").withRenderingMode(.alwaysTemplate)
        case .wind:
            return #imageLiteral(resourceName: "Wind").withRenderingMode(.alwaysTemplate)
        case .fog:
            return #imageLiteral(resourceName: "Fog").withRenderingMode(.alwaysTemplate)
        case .cloudy:
            return #imageLiteral(resourceName: "Cloudy").withRenderingMode(.alwaysTemplate)
        case .partlyCloudy:
            return #imageLiteral(resourceName: "PartlyCloudy").withRenderingMode(.alwaysTemplate)
        }
    }
}
